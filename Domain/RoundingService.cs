﻿using System;

namespace Domain.Services
{
    public class RoundingService
    {
        public static decimal Round(decimal number)
        {
            return Math.Round(number*2, MidpointRounding.AwayFromZero)/2;
        }
    }
}
