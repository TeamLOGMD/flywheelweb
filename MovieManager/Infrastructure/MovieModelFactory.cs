﻿using System.Collections.Generic;
using AutoMapper;
using Domain.Services;
using Models;
using MovieManager.Models;

namespace MovieManager.Infrastructure
{
    public class MovieModelFactory
    {
        private readonly IRuntimeMapper _mapper;

        public MovieModelFactory(IRuntimeMapper mapper = null)
        {
            _mapper = mapper ?? GetMapper();
        }

        private IRuntimeMapper GetMapper()
        {
            var mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MovieDto, MovieModel>()
                    .ForMember(d => d.YearOfRelease, m => m.MapFrom(s => s.ReleaseDate.Year))
                    .ForMember(d => d.AverageRating, m => m.MapFrom(s => RoundingService.Round(s.AverageRating)));
            }));

            return mapper;
        }

        public MovieModel Create(MovieDto dto)
        {
            return _mapper.Map<MovieModel>(dto);
        }

        public List<MovieModel> Create(List<MovieDto> dtos)
        {
            return _mapper.Map<List<MovieModel>>(dtos);
        }
    }
}