﻿using Infrastructure.Validators;
using MovieManager.Infrastructure.Contracts;

namespace MovieManager.Infrastructure.Validators
{
    public class GetTopUserMoviesRequestValidator:IValidator<GetTopUserMoviesRequest>
    {
        public bool Validate(GetTopUserMoviesRequest source)
        {
            if (source == null || source.UserId <= 0) return false;

            return true;
        }

        public static GetTopUserMoviesRequestValidator Create() => new GetTopUserMoviesRequestValidator();
    }
}