﻿using Infrastructure.Validators;
using MovieManager.Infrastructure.Contracts.Ratings;

namespace MovieManager.Infrastructure.Validators
{
    public class RateMovieRequestValidator:IValidator<RateMovieRequest>
    {
        public bool Validate(RateMovieRequest source)
        {
            if (source.Value <= 0 || source.Value > 5) return false;
            return true;
        }

        public static RateMovieRequestValidator Create() => new RateMovieRequestValidator();
    }
}