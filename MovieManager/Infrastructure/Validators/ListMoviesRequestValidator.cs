﻿using System.Linq;
using Infrastructure.Validators;
using MovieManager.Infrastructure.Contracts;

namespace MovieManager.Infrastructure.Validators
{
    public class ListMoviesRequestValidator : IValidator<ListMoviesRequest>
    {
        public bool Validate(ListMoviesRequest source)
        {
            if (source == null) return false;

            var hasTitle = !string.IsNullOrWhiteSpace(source.Title);
            var hasYearOfRelease = source.YearOfRelease.HasValue;
            var hasGenres = source.Genres!=null && source.Genres.Any();

            if (!(hasTitle || hasYearOfRelease || hasGenres)) return false;

            return true;
        }

        public static ListMoviesRequestValidator Create()
        {
            return new ListMoviesRequestValidator();
        }
    }
}