﻿namespace MovieManager.Infrastructure.Contracts.Ratings
{
    public class RateMovieRequest
    {
        public int UserId { get; set; }

        public int MovieId { get; set; }

        public int Value { get; set; }
    }
}