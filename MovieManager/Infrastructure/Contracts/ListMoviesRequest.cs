﻿using System.Collections.Generic;
using Infrastructure.Enums;

namespace MovieManager.Infrastructure.Contracts
{
    public class ListMoviesRequest
    {
        public string Title { get; set; }

        public int? YearOfRelease { get; set; }

        public List<Genre> Genres { get; set; }
    }
}