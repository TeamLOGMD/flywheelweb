﻿namespace MovieManager.Infrastructure.Contracts
{
    public class GetTopUserMoviesRequest
    {
        public int UserId { get; set; }
    }
}