﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using Infrastructure.Enums;
using Models;
using Moq;
using MovieManager.Infrastructure;
using MovieManager.Infrastructure.Contracts;
using MovieManager.Infrastructure.Contracts.Ratings;
using MovieManager.Infrastructure.Validators;
using Repository.Contracts;
using Repository.Interfaces;
using Repository.Repositories;

namespace MovieManager.Controllers
{
    public class MoviesController : ApiController
    {
        private readonly IMovieWriteRepository _movieWriteRepository;
        private readonly MovieModelFactory _modelFactory;
        private readonly IMovieReadRepository _movieReadRepository;

        protected MoviesController() : this(null)
        {
        }

        protected MoviesController(IMovieReadRepository movieReadRepository = null, IMovieWriteRepository movieWriteRepository = null, MovieModelFactory modelFactory = null)
        {
            var mockWriteRepo = new Mock<IMovieWriteRepository>();

            _movieWriteRepository = movieWriteRepository?? mockWriteRepo.Object ;
            _movieReadRepository = movieReadRepository ?? new MovieReadRepository();

            _modelFactory = modelFactory ?? new MovieModelFactory();
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetMovies([FromUri] ListMoviesRequest request)
        {
            var result = ListMoviesRequestValidator.Create().Validate(request);

            if (!result) throw new HttpResponseException(HttpStatusCode.BadRequest);

            var options = new ListMovieOptions
            {
                Title = request.Title,
                Genres = request.Genres??new List<Genre>(),
                YearOfRelease = request.YearOfRelease
            };

            var movies = await _movieReadRepository.List(options).ConfigureAwait(false);

            ValidateNumberOfMovies(movies);

            return Ok(_modelFactory.Create(movies));
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetTopMovies()
        {
            var options = new ListMoviesByRatingOptions();

            var movies = await _movieReadRepository.ListByRating(options);

            ValidateNumberOfMovies(movies);

            return Ok(_modelFactory.Create(movies));
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetTopUserMovies([FromUri] GetTopUserMoviesRequest request)
        {
            var isValid = GetTopUserMoviesRequestValidator.Create().Validate(request);

            if (!isValid) throw new HttpResponseException(HttpStatusCode.BadRequest);

            var options = new ListMoviesByRatingOptions
            {
                UserId = request.UserId
            };

            var movies = await _movieReadRepository.ListByRating(options);

            ValidateNumberOfMovies(movies);

            return Ok(_modelFactory.Create(movies));
        }

        [HttpPost]
        public async Task<IHttpActionResult> SaveRating([FromUri] RateMovieRequest request)
        {
            var isValid = RateMovieRequestValidator.Create().Validate(request);

            if(!isValid) throw new HttpResponseException(HttpStatusCode.BadRequest);

            var findRatingOptions = new FineMovieOptions
            {
                MovieId = request.MovieId,
            };

            var movie = await _movieWriteRepository.FindMovie(findRatingOptions);

            if (movie == null) throw new HttpResponseException(HttpStatusCode.NotFound);

            movie.SetUserRating(request.UserId,request.Value);

            var result = await _movieWriteRepository.UpdateMovie(movie);

            if (!result) throw new HttpResponseException(HttpStatusCode.BadRequest);

            return Ok();
        }

        private void ValidateNumberOfMovies(List<MovieDto> movies)
        {
            if (movies == null || !movies.Any()) throw new HttpResponseException(HttpStatusCode.NotFound);
        }
    }
}