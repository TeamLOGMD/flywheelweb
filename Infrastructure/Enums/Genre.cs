﻿namespace Infrastructure.Enums
{
    public enum Genre
    {
        Action,
        Thriller
    }
}