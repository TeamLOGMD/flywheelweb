﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Models;
using Repository.Contracts;

namespace Repository.Interfaces
{
    public interface IMovieReadRepository
    {
        Task<List<MovieDto>> List(ListMovieOptions options);

        Task<List<MovieDto>> ListByRating(ListMoviesByRatingOptions options);

        Task<bool> Exists(int movieId);
    }
}