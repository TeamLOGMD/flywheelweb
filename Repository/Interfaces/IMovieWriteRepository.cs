﻿using System.Threading.Tasks;
using Domain.Models.Movies;
using Repository.Contracts;

namespace Repository.Interfaces
{
    public interface IMovieWriteRepository
    {
        Task<MovieAggregateRoot> FindMovie(FineMovieOptions options);

        Task<bool> UpdateMovie(MovieAggregateRoot movie);
    }
}
