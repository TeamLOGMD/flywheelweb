﻿using System.Collections.Generic;
using System.Linq;
using Infrastructure.Enums;

namespace Repository.Contracts
{
   public class ListMovieOptions
    {
        public string Title { get; set; }

        public int? YearOfRelease { get; set; }

        public List<Genre> Genres { get; set; }

        public bool HasGenres => Genres != null && Genres.Any();
    }
}
