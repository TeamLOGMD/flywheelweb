﻿namespace Repository.Contracts
{
    public class ListMoviesByRatingOptions
    {
        public int ResultSize { get; set; } = 5;

        public int? UserId { get; set; }
    }
}