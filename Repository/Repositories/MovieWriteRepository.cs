﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Models.Movies;
using Movie.EntityFramework;
using Movie.EntityFramework.Entities;
using Repository.Contracts;
using Repository.Interfaces;

namespace Repository.Repositories
{
    public class MovieWriteRepository : IMovieWriteRepository
    {
        private readonly MovieDbContext _dbContext;
        private readonly IRuntimeMapper _mapper;

        public MovieWriteRepository(MovieDbContext dbContext = null, IRuntimeMapper mapper = null)
        {
            _dbContext = dbContext ?? new MovieDbContext();
            _mapper = mapper ?? GetMapper();
        }

        public async Task<MovieAggregateRoot> FindMovie(FineMovieOptions options)
        {
            var movie = await _dbContext
                .Movies
                .Include(m => m.Ratings)
                .FirstOrDefaultAsync(m => m.Id == options.MovieId);

            if (movie == null) return null;

            return _mapper.Map<MovieAggregateRoot>(movie);
        }

        public Task<bool> UpdateMovie(MovieAggregateRoot movie)
        {
            throw new NotImplementedException();
        }

        private IRuntimeMapper GetMapper()
        {
            var mapper = new Mapper(new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MovieDao, MovieAggregateRoot>();

                cfg.CreateMap<RatingDao, RatingDomainEntity>();
            }));

            return mapper;
        }
    }
}