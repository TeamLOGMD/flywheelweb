﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Models;
using Movie.EntityFramework;
using Movie.EntityFramework.Entities;
using Repository.Contracts;
using Repository.Interfaces;

namespace Repository.Repositories
{
    public class MovieReadRepository : IMovieReadRepository
    {
        private readonly MovieDbContext _dbContext;
        private readonly IRuntimeMapper _mapper;

        public MovieReadRepository(MovieDbContext dbContext = null, IRuntimeMapper mapper = null)
        {
            _dbContext = dbContext ?? new MovieDbContext();
            _mapper = mapper ?? GetMapper();
        }

        public async Task<List<MovieDto>> List(ListMovieOptions options)
        {
            var query = _dbContext.Movies
                .Include(m => m.Ratings)
                .Where(m =>
                    string.IsNullOrEmpty(options.Title) || m.Title.Contains(options.Title) &&
                    !options.HasGenres || options.Genres.Contains(m.Genre) &&
                    !options.YearOfRelease.HasValue || m.ReleaseDate.Year == options.YearOfRelease);

            var result = await query.ToListAsync();

            return _mapper.Map<List<MovieDto>>(result);
        }

        public async Task<List<MovieDto>> ListByRating(ListMoviesByRatingOptions options)
        {
            var query = _dbContext.Movies
                .Include(m => m.Ratings)
                .Where(m => m.Ratings.Any())
                .Select(m => new
                {
                    Dao = m,
                    Average = m.Ratings.Where(r => !options.UserId.HasValue || r.UserId == options.UserId).ToList().Average(r => r.Rating)
                })
                .OrderByDescending(m => m.Average)
                .ThenBy(m => m.Dao.Title)
                .Take(options.ResultSize);

            var result = await query.ToListAsync();

            return result.Select(m =>
            {
                var mapped = _mapper.Map<MovieDto>(m.Dao);
                mapped.AverageRating = m.Average;
                return mapped;
            }).ToList();
        }

        public Task<bool> Exists(int movieId)
        {
            return _dbContext.Movies.AnyAsync(m => m.Id == movieId);
        }

        private IRuntimeMapper GetMapper()
        {
            var mapper = new Mapper(new MapperConfiguration(cfg => { cfg.CreateMap<MovieDao, MovieDto>(); }));

            return mapper;
        }
    }
}