﻿using Domain.Services;
using Xunit;

namespace Domain.Tests
{
    public class RoundingServiceTests
    {
        [Theory]
        [InlineData(2.91, 3)]
        [InlineData(3.25, 3.5)]
        [InlineData(3.75, 4)]
        public void RoundUp(decimal number, decimal expected)
        {
            var result = RoundingService.Round(number);

            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData(3.249, 3)]
        [InlineData(3.6, 3.5)]
        public void RoundDown(decimal number, decimal expected)
        {
            var result = RoundingService.Round(number);

            Assert.Equal(expected, result);
        }

        [Theory]
        [InlineData(1)]
        [InlineData(2)]
        [InlineData(3)]
        [InlineData(4)]
        [InlineData(5)]
        public void NoRounding(decimal number)
        {
            var result = RoundingService.Round(number);

            Assert.Equal(number, result);
        }
    }
}