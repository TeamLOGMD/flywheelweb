﻿using System;
using Infrastructure.Enums;

namespace Models
{
    public class MovieDto
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public Genre Genre { get; set; }

        public DateTime ReleaseDate { get; set; }

        public int RunningTime { get; set; }

        public decimal AverageRating { get; set; }
    }
}