﻿namespace Models
{
    public class RatingDto
    {
        public int MovieId { get; set; }

        public int Rating { get; set; }

        public int UserId { get; set; }
    }
}
