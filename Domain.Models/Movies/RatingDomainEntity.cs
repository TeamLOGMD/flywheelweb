﻿namespace Domain.Models.Movies
{
    public class RatingDomainEntity
    {
        public int UserId { get; set; }

        public int Value { get; set; }
    }
}