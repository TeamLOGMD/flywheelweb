﻿using System.Collections.Generic;
using System.Linq;

namespace Domain.Models.Movies
{
    public class MovieAggregateRoot
    {
        public int Id { get; set; }

        public ICollection<RatingDomainEntity> Ratings { get; set; } = new List<RatingDomainEntity>();

        public void SetUserRating(int userId, int ratingValue)
        {
            if (ratingValue < 0 || ratingValue > 5) return;

            var rating = Ratings.FirstOrDefault(r => r.UserId == userId)
                         ?? new RatingDomainEntity();

            rating.Value = ratingValue;
        }
    }
}