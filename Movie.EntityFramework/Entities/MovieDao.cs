﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Infrastructure.Enums;

namespace Movie.EntityFramework.Entities
{
    public class MovieDao
    {
        [Key]
        public int Id { get; set; }

        public Genre Genre { get; set; }

        public string Title { get; set; }

        public DateTime ReleaseDate { get; set; }

        public int RunningTime { get; set; }

        [InverseProperty(nameof(RatingDao.Movie))]
        public virtual ICollection<RatingDao> Ratings { get; set; } = new List<RatingDao>();
    }
}
