﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Movie.EntityFramework.Entities
{
    public class UserDao
    {
        [Key]
        public int Id { get; set; }

        [InverseProperty(nameof(RatingDao.User))]
        public virtual ICollection<RatingDao> MovieRatings { get; set; }
    }
}