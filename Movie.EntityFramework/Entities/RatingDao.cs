﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Movie.EntityFramework.Entities
{
    public class RatingDao
    {
        [Key]
        public int Id { get; set; }

        public int UserId { get; set; }

        public int MovieId { get; set; }

        public decimal Rating { get; set; }

        [ForeignKey(nameof(UserId))]
        public virtual UserDao User { get; set; }

        [ForeignKey(nameof(MovieId))]
        public virtual MovieDao Movie { get; set; }
    }
}