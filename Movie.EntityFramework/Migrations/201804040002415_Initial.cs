namespace Movie.EntityFramework.Migrations
{
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MovieDaos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Genre = c.Int(nullable: false),
                        Title = c.String(),
                        ReleaseDate = c.DateTime(nullable: false),
                        RunningTime = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.RatingDaos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Int(nullable: false),
                        MovieId = c.Int(nullable: false),
                        Rating = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserDaos", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.MovieDaos", t => t.MovieId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.MovieId);
            
            CreateTable(
                "dbo.UserDaos",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RatingDaos", "MovieId", "dbo.MovieDaos");
            DropForeignKey("dbo.RatingDaos", "UserId", "dbo.UserDaos");
            DropIndex("dbo.RatingDaos", new[] { "MovieId" });
            DropIndex("dbo.RatingDaos", new[] { "UserId" });
            DropTable("dbo.UserDaos");
            DropTable("dbo.RatingDaos");
            DropTable("dbo.MovieDaos");
        }
    }
}
