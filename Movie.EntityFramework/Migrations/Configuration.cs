using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using Infrastructure.Enums;
using Movie.EntityFramework.Entities;

namespace Movie.EntityFramework.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<MovieDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MovieDbContext context)
        {
            if (context.Movies.Any()) return;

            var user1 = new UserDao();
            var user2 = new UserDao();
            var user3 = new UserDao();

            var users = new List<UserDao>
            {
                user1,
                user2,
                user3
            };

            context.Users.AddRange(users);
            context.SaveChanges();

            var movies = new List<MovieDao>
            {
                new MovieDao
                {
                    Title = "Movie 1",
                    Genre = Genre.Action,
                    RunningTime = 129,
                    ReleaseDate = new DateTime(2007, 6, 6)
                },
                new MovieDao
                {
                    Title = "Movie 2",
                    Genre = Genre.Action,
                    RunningTime = 229,
                    ReleaseDate = new DateTime(2006, 5, 5)
                },
                new MovieDao
                {
                    Title = "Movie 3",
                    Genre = Genre.Thriller,
                    RunningTime = 109,
                    ReleaseDate = new DateTime(2016, 1, 1)
                },
                new MovieDao
                {
                    Title = "Movie 4",
                    Genre = Genre.Thriller,
                    RunningTime = 100,
                    ReleaseDate = new DateTime(2014, 1, 1)
                }
            };

            context.Movies.AddRange(movies);
            context.SaveChanges();

            var ratings = new List<RatingDao>
            {
                new RatingDao {UserId = 1, MovieId = 1, Rating = 1},
                new RatingDao {UserId = 2, MovieId = 1, Rating = 2},
                new RatingDao {UserId = 1, MovieId = 2, Rating = 3},
                new RatingDao {UserId = 2, MovieId = 2, Rating = 4},
                new RatingDao {UserId = 1, MovieId = 3, Rating = 5},
                new RatingDao {UserId = 2, MovieId = 3, Rating = 4},
                new RatingDao {UserId = 2, MovieId = 3, Rating = 1}
            };

            context.Ratings.AddRange(ratings);
            context.SaveChanges();
        }
    }
}