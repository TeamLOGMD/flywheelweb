﻿using System.Data.Entity;
using Movie.EntityFramework.Entities;

namespace Movie.EntityFramework
{
    public class MovieDbContext : DbContext
    {
        public MovieDbContext() : base("name=MovieDBContext")
        {
        }

        public DbSet<MovieDao> Movies { get; set; }

        public DbSet<UserDao> Users { get; set; }

        public DbSet<RatingDao> Ratings { get; set; }
    }
}